#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate rocket;
extern crate postgres;

use std::env::var;

mod datatypes;
mod endpoints;

lazy_static! {
    /// This is an example for using doc comment attributes
    static ref CONNSTR: String =
        format!(
            "postgres://postgres:{}@{}/fridgeshare",
            var("DB_ADDRESS").unwrap(),
            var("DB_PASSWORD").unwrap(),
        );
}

fn main() {
    //let conn = Connection::connect(CONNSTR, TlsMode::None).ok();
    //test_db(conn);
    rocket::ignite()
        .mount(
            "/",
            routes![
                endpoints::check_db,
                endpoints::register,
                endpoints::cleardb,
                endpoints::login,
                endpoints::get_fridge,
                endpoints::add_user_to_fridge,
                endpoints::update_fridge,
                endpoints::clear_fridge,
                endpoints::update_list,
                endpoints::create_list,
                endpoints::delete_list,
                endpoints::join_list,
                endpoints::get_lists,
                endpoints::get_list_by_id,
                endpoints::complete_that_frikkin_list
            ],
        )
        .launch();
}
