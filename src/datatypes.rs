use rocket_contrib::json::Json;
use serde::Serialize;
use serde::Deserialize;

#[derive(Serialize, Deserialize)]
pub struct FoodItem {
    pub foodID: i32,
    pub name: String,
    pub quantity: i32,
    pub measure: Measure
}

#[derive(Serialize, Deserialize)]
pub struct FoodItemJson {
    pub name: String,
    pub quantity: i32,
    pub measure: String
}

#[derive(Serialize, Deserialize)]
pub struct Fridge {
    pub fridgeID: i32,
    pub contents: Vec<i32>
}

#[derive(Serialize, Deserialize)]
pub struct FridgeJson{
    pub fridgeID: i32,
    pub contents: Vec<FoodItem>
}

#[derive(Serialize, Deserialize)]
pub struct ShoppingList {
    pub listID: i32,
    pub name: String,
    pub shitToBuy: Vec<FoodItem>,
    pub users: Vec<User>
}

#[derive(Serialize, Deserialize)]
pub struct ShoppingListJson {
    pub listID: i32,
    pub name: String,
    pub shitToBuy: Vec<FoodItem>,
    pub users: Vec<UserJson>
}



#[derive(Serialize, Deserialize)]
pub struct User {
    pub userID: i32,
    pub userName: String,
    pub password: String,
    pub fridgeID: i32
}

#[derive(Serialize, Deserialize)]
pub struct UserJson {
    pub userID: i32,
    pub userName: String,
}

#[derive(Serialize, Deserialize)]
pub enum Measure {
    gram,
    litre,
    piece
}

pub fn str_to_measure(s: String) -> Measure{
    if s.eq(&"gram".to_string()){
        Measure::gram
    } else if s.eq(&"litre".to_string()) {
        Measure::litre
    } else {
        Measure::piece
    }
}

////////////
//only backend shit, titeket nem érdekel, ezeket a droidokat nem keresitek
////////////
#[derive(Serialize, Deserialize)]
pub struct Userinfo{
    pub username: String,
    pub password: String
}