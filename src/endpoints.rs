use crate::datatypes;
use crate::datatypes::{str_to_measure, FoodItem, FoodItemJson, Fridge, FridgeJson, Measure, User, UserJson, ShoppingList, ShoppingListJson};
use crate::CONNSTR;
use postgres::{Connection, TlsMode};
use rocket::response::status;
use rocket_contrib::json::{Json, JsonValue};
use std::io;
use std::path::Path;

use postgres::rows::Row;
use rocket::http::RawStr;
use rocket::Data;


#[get("/checkdb")]
pub fn check_db() -> JsonValue {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None).ok();
    match conn {
        None => json!({
            "Status" : "fucked"
        }),
        Some(conn) => {
            Connection::finish(conn);
            json!({
                "Status" : "seems ok"
            })
        }
    }
}

#[get("/cleardb")]
pub fn cleardb() -> Result<JsonValue, status::NotFound<String>> {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None).ok();
    match conn {
        None => Err(status::NotFound("No DB connection :(".to_string())),
        Some(conn) => {
            &conn.execute("TRUNCATE public.user, public.fridge, public.shoppinglist, public.fooditem RESTART IDENTITY", &[]).ok();
            Connection::finish(conn);
            Ok(json!({
                "result" : "the database was cleaned of all your filthy data... please next time be more careful!"
            }))
        }
    }
}

#[post("/register", format = "json", data = "<userinfo>")]
pub fn register(
    userinfo: Json<datatypes::Userinfo>,
) -> Result<JsonValue, status::NotFound<String>> {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None).ok();
    match conn {
        None => Err(status::NotFound("No DB connection :(".to_string())),

        Some(conn) => {
            let rows_affected = &conn
                .execute(
                    "INSERT INTO public.user (username, password, activelistid) VALUES ($1, $2, $3)",
                    &[&userinfo.username, &userinfo.password, &Vec::<i32>::new()],
                )
                .ok();
            match rows_affected {
                None => {
                    Connection::finish(conn);
                    Err(status::NotFound("User already exists!".to_string()))
                }

                Some(r) => {
                    let temp: Vec<i32> = vec![];
                    let data0 = &conn
                        .query(
                            "SELECT id FROM public.user WHERE username=$1",
                            &[&userinfo.username],
                        )
                        .unwrap();
                    let temp_uid: i32 = data0.get(0).get(0);

                    &conn
                        .execute(
                            "INSERT INTO public.fridge (content, name) VALUES ($1, $2)",
                            &[
                                &temp,
                                &format!("default fridge for username: {}", userinfo.username),
                            ],
                        )
                        .unwrap();

                    let data0 = &conn
                        .query(
                            "SELECT id FROM public.fridge WHERE name=$1",
                            &[&format!(
                                "default fridge for username: {}",
                                userinfo.username
                            )],
                        )
                        .ok();
                    let mut temp_fid: i32;
                    match data0 {
                        None => Err(status::NotFound("Something went wrong :(".to_string())),
                        Some(data0) => {
                            temp_fid = data0.get(0).get(0);
                            &conn
                                .execute(
                                    "UPDATE public.user SET fridgeid=$1 WHERE id=$2",
                                    &[&temp_fid, &temp_fid],
                                )
                                .ok();
                            Ok(json!({ "registered": format!("{}", temp_uid) }))
                        }
                    }
                }
            }
        }
    }
}

#[post("/login", format = "json", data = "<userinfo>")]
pub fn login(userinfo: Json<datatypes::Userinfo>) -> Result<JsonValue, status::NotFound<String>> {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None).ok();
    match conn {
        None => Err(status::NotFound("No DB connection :(".to_string())),

        Some(conn) => {
            let results = conn
                .query(
                    "SELECT id FROM public.user WHERE username = $1 AND password = $2",
                    &[&userinfo.username, &userinfo.password],
                )
                .ok();
            let mut uid: i32;
            Connection::finish(conn);
            match results {
                Some(res) => {
                    uid = res.get(0).get(0);
                    Ok(json!({ "token": format!("{}", uid) }))
                }
                None => Err(status::NotFound("Wrong login credentials".to_string())),
            }
        }
    }
}

#[get("/user/<uid>/fridge")]
pub fn get_fridge(uid: i32) -> Result<Json<FridgeJson>, status::NotFound<String>> {
    let mut final_fridge: Fridge;
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None).ok();
    match conn {
        None => Err(status::NotFound("No DB connection :(".to_string())),
        Some(conn) => {
            let res = &conn
                .query("SELECT fridgeid FROM public.user WHERE id=$1", &[&uid])
                .ok(); //this
            match res {
                None => {
                    Connection::finish(conn);
                    Err(status::NotFound(format!(
                        "No user was found with this ID: {}",
                        uid
                    )))
                }
                Some(res) => {
                    let param: i32 = res.get(0).get(0);
                    let res2 = &conn
                        .query(
                            "SELECT id, content FROM public.fridge where id=$1",
                            &[&param],
                        )
                        .ok();
                    Connection::finish(conn);
                    match res2 {
                        None => Err(status::NotFound(format!(
                            "No fridge was found with ID: {} ",
                            param
                        ))),
                        Some(res2) => {
                            final_fridge = Fridge {
                                fridgeID: res2.get(0).get(0),
                                contents: res2.get(0).get(1),
                            };
                            Ok(fridge_to_json(final_fridge))
                            //comment
                        }
                    }
                }
            }
        }
    }
}

#[get("/user/<uid>/fridge/connect/<fid>")]
pub fn add_user_to_fridge(uid: i32, fid: i32) -> Result<JsonValue, status::NotFound<String>> {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None).ok();
    match conn {
        None => Err(status::NotFound("No DB connection :(".to_string())),
        Some(conn) => {
            let res = &conn
                .query(
                    "UPDATE public.user SET fridgeid = $1 WHERE id=$2",
                    &[&fid, &uid],
                )
                .ok();
            match res {
                None => Err(status::NotFound("valami eltört :(".to_string())),
                Some(res) => Ok(json!({
                    "result" : "fridge {} was set as default fridge for user {}"
                })),
            }
        }
    }
}

fn fridge_to_json(f: Fridge) -> Json<FridgeJson> {
    let mut res = FridgeJson {
        fridgeID: f.fridgeID,
        contents: vec![],
    };
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None).ok();
    match conn {
        None => {}
        Some(conn) => {
            for fid in f.contents {
                let queryres = &conn.query("SELECT foodid, name, quantity, measure FROM public.fooditem WHERE foodid=$1", &[&fid]).ok();
                match queryres {
                    None => {
                        panic!("faszom");
                    }
                    Some(queryres) => {
                        for row in queryres {
                            res.contents.push(FoodItem {
                                foodID: row.get(0),
                                name: row.get(1),
                                quantity: row.get(2),
                                measure: str_to_measure(row.get(3)),
                            });
                        }
                    }
                }
            }
        }
    }
    Json(res)
}

#[post("/user/<uid>/fridge/update/<fid>", format = "json", data = "<items>")]
pub fn update_fridge(
    uid: i32,
    fid: i32,
    items: Json<Vec<FoodItemJson>>,
) -> Result<(), status::NotFound<String>> {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;

    conn.query("SELECT fridgeid FROM public.user WHERE id=$1", &[&uid])
        .map_err(|_| status::NotFound("No fridge found".into()))?;

    conn.execute(
        "UPDATE public.fridge SET content = $1 WHERE id=$2",
        &[&Vec::<i32>::new(), &fid],
    )
    .map_err(|e| status::NotFound(e.to_string()))?;

    for item in items.into_inner() {
        conn.execute(
            "INSERT INTO public.fooditem (name, quantity, measure) VALUES ($1,$2,$3)",
            &[&item.name, &item.quantity, &item.measure],
        )
        .map_err(|e| status::NotFound(e.to_string()))?;

        let last_id: i32 = conn
            .query("SELECT max(id) FROM public.fooditem", &[])
            .map_err(|e| status::NotFound(e.to_string()))?
            .get(0)
            .get(0);

        &conn
            .execute(
                "UPDATE public.fridge SET content = array_append(content, $1) WHERE id = $2",
                &[&last_id, &fid],
            )
            .map_err(|e| status::NotFound(e.to_string()))?;
    }
    Ok(())
}

#[get("/user/<uid>/fridge/clear/<fid>")]
pub fn clear_fridge(uid: i32, fid: i32) -> Result<JsonValue, status::NotFound<String>> {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None).ok();
    match conn {
        None => Err(status::NotFound("No DB connection :(".to_string())),
        Some(conn) => {
            let mut clean_af: Vec<i32>;
            clean_af = vec![];
            &conn
                .execute(
                    "UPDATE public.fridge SET content = $1 WHERE id=$2",
                    &[&clean_af, &fid],
                )
                .unwrap();
            Ok(json!({
                "result": format!("Fridge cleared for user {}, fridge {}", uid, fid)
            }))
        }
    }
}

#[post("/user/<uid>/list/update/<lid>", format = "json", data = "<items>")]
pub fn update_list(
    uid: i32,
    lid: i32,
    items: Json<Vec<FoodItemJson>>,
) -> Result<(), status::NotFound<String>> {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;


    //clear the list
    conn.execute(
        "UPDATE public.shoppinglist SET items = $1 WHERE id=$2",
        &[&Vec::<i32>::new(), &lid],
    )
        .map_err(|e| status::NotFound(e.to_string()))?;


    for item in items.into_inner() {
        conn.execute(
            "INSERT INTO public.fooditem (name, quantity, measure) VALUES ($1,$2,$3)",
            &[&item.name, &item.quantity, &item.measure],
        )
            .map_err(|e| status::NotFound(e.to_string()))?;

        let last_id: i32 = conn
            .query("SELECT max(id) FROM public.fooditem", &[])
            .map_err(|e| status::NotFound(e.to_string()))?
            .get(0)
            .get(0);

        &conn
            .execute(
                "UPDATE public.shoppinglist SET items = array_append(items, $1) WHERE id = $2",
                &[&last_id, &lid],
            )
            .map_err(|e| status::NotFound(e.to_string()))?;
    }
    Ok(())
}


#[get("/user/<uid>/list/create/<listname>")]
pub fn create_list(
    uid: i32,
    listname: String
)-> Result<(), status::NotFound<String>>{
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;

    conn.execute(
        "INSERT INTO public.shoppinglist (name, items, users) VALUES ($1, $2, $3)",
        &[&listname, &Vec::<i32>::new(), &vec![uid]],
    )
        .map_err(|e| status::NotFound(e.to_string()))?;

    let last_id: i32 = conn
        .query("SELECT max(id) FROM public.shoppinglist", &[])
        .map_err(|e| status::NotFound(e.to_string()))?
        .get(0)
        .get(0);

    &conn
        .execute(
            "UPDATE public.user SET activelistid = array_append(activelistid, $1) WHERE id = $2",
            &[&last_id, &uid],
        )
        .map_err(|e| status::NotFound(e.to_string()))?;
    Ok(())
}

#[delete("/user/<uid>/list/delete/<lid>")]
pub fn delete_list(uid: i32, lid: i32)-> Result<(), status::NotFound<String>>{
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;

    conn.execute(
        "DELETE FROM public.shoppinglist WHERE id=$1",
        &[&lid],
    )
        .map_err(|e| status::NotFound(e.to_string()))?;

    &conn
        .execute(
            "UPDATE public.user SET activelistid = array_remove(activelistid, $1) WHERE id = $2",
            &[&lid, &uid],
        )
        .map_err(|e| status::NotFound(e.to_string()))?;
    Ok(())
}

#[delete("/user/<uid>/list/join/<lid>")]
pub fn join_list(uid: i32, lid: i32) -> Result<(), status::NotFound<String>>{

    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;

    conn.execute(
        "UPDATE public.user SET activelistid = array_append(activelistid, $1) WHERE id=$2",
        &[&lid, &uid],
    ).map_err(|e| status::NotFound(e.to_string()))?;

    conn.execute(
        "UPDATE public.shoppinglist SET users = array_append(users, $1) WHERE id=$2",
        &[&uid, &lid],
    ).map_err(|e| status::NotFound(e.to_string()))?;

    Ok(())
}
#[get("/user/<uid>/list")]
pub fn get_lists(uid: i32)-> Result<Json<Vec<ShoppingListJson>>, status::NotFound<String>>{
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;
    let mut res = Vec::<ShoppingListJson>::new();
    let rows = &conn
        .query(
            "SELECT activelistid FROM public.user WHERE id=$1",
            &[&uid],
        ).map_err(|e| status::NotFound(e.to_string()))?;
        let lists: Vec<i32> = rows.get(0).get(0);
        if lists.len() > 0 {
            for listid in lists {
                match get_list_by_id_helper(listid){
                    None => {

                    }
                    Some(listyboi) =>{
                        res.push(listyboi);
                    }
                }

            }
            Ok(Json(res))
        } else {
            Err(status::NotFound("shieeet just hit the fan hard".to_string()))
        }
}

#[get("/user/<uid>/list/<lid>")]
pub fn get_list_by_id(uid: i32, lid: i32)-> Result<Json<ShoppingListJson>, status::NotFound<String>>{

    match get_list_by_id_helper(lid){
        None =>{
            Err(status::NotFound("shieeet".to_string()))
        }
        Some(res) =>{
            Ok(Json(res))
        }
    }
}


#[get("/user/<uid>/list/complete/<lid>")]
pub fn complete_that_frikkin_list(uid: i32, lid: i32)-> Result<(), status::NotFound<String>>{
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;

    let res =
        &conn.query(
            "SELECT items FROM public.shoppinglist WHERE id=$1",&[&lid],
        ).map_err(|e| status::NotFound(e.to_string()))?;
    let itemidlist :Vec<i32> = res.get(0).get(0);
    &conn.execute(
        "UPDATE public.user SET activelistid = array_remove(activelistid, $1) WHERE id=$2",&[&lid, &uid],
    ).map_err(|e| status::NotFound(e.to_string()))?;

    let res = &conn.query(
        "SELECT fridgeid from public.user WHERE id=$1",&[&uid],
    ).map_err(|e| status::NotFound(e.to_string()))?;
    let fridge_id: i32 = res.get(0).get(0);
    for itemidlistelement in itemidlist {
        let res = &conn.execute(
        "UPDATE public.fridge SET content = array_append(content, $1) WHERE id=$2", &[&itemidlistelement, &uid],
        ).map_err(|e| status::NotFound(e.to_string()))?;
    }
    &conn.execute(
        "DELETE FROM public.shoppinglist WHERE id=$1",&[&lid],
    ).map_err(|e| status::NotFound(e.to_string()))?;

    Ok(())
}


fn get_list_by_id_helper(lid: i32) -> Option<ShoppingListJson>{
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .unwrap();
    let mut res = Vec::<ShoppingListJson>::new();

    let temp_rows = &conn.query(
        "SELECT name, items, users FROM public.shoppinglist WHERE id=$1", &[&lid]
    ).unwrap();

    let itemvec: Vec<i32> = temp_rows.get(0).get(1);
    let mut fooditem_res_vec = Vec::<FoodItem>::new();
    for item in itemvec {
        let temp_fooditems = &conn.query
        ("SELECT id, name, quantity, measure FROM public.fooditem WHERE id=$1",
         &[&item]
        ).unwrap();
        fooditem_res_vec.push(FoodItem{
            foodID: temp_fooditems.get(0).get(0),
            name: temp_fooditems.get(0).get(1),
            quantity: temp_fooditems.get(0).get(2),
            measure: datatypes::str_to_measure(temp_fooditems.get(0).get(3))
        })
    }
    let uservec: Vec<i32> = temp_rows.get(0).get(2);
    let mut useritem_res_vec = Vec::<UserJson>::new();
    for user in uservec {
        let temp_users =  &conn.query
        ("SELECT id, username FROM public.user WHERE id=$1",
         &[&user]
        ).unwrap();
        useritem_res_vec.push(UserJson{
            userID: temp_users.get(0).get(0),
            userName: temp_users.get(0).get(1)
        })
    }
    Some(ShoppingListJson{
        listID: lid,
        name: temp_rows.get(0).get(0),
        shitToBuy: fooditem_res_vec,
        users: useritem_res_vec
    })
}









//csak hogy meglegyen a magic feri_style_kelgyó
/*
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;
    let mut res = Vec::<ShoppingListJson>::new();

    let temp_rows = &conn.query(
        "SELECT name, items, users FROM public.shoppinglist WHERE id=$1", &[&lid]
    ).map_err(|_| status::NotFound("No DB connection :(".into()))?;

    let itemvec: Vec<i32> = temp_rows.get(0).get(1);
    let mut fooditem_res_vec = Vec::<FoodItem>::new();
    for item in itemvec {
        let temp_fooditems = &conn.query
        ("SELECT id, name, quantity, measure FROM public.fooditem WHERE id=$1",
         &[&item]
        ).map_err(|_| status::NotFound("No DB connection :(".into()))?;
        fooditem_res_vec.push(FoodItem{
            foodID: temp_fooditems.get(0).get(0),
            name: temp_fooditems.get(0).get(1),
            quantity: temp_fooditems.get(0).get(2),
            measure: datatypes::str_to_measure(temp_fooditems.get(0).get(3))
        })
    }
    let uservec: Vec<i32> = temp_rows.get(0).get(2);
    let mut useritem_res_vec = Vec::<UserJson>::new();
    for user in uservec {
        let temp_users =  &conn.query
        ("SELECT id, username FROM public.user WHERE id=$1",
         &[&user]
        ).map_err(|_| status::NotFound("No DB connection :(".into()))?;
        useritem_res_vec.push(UserJson{
            userID: temp_users.get(0).get(0),
            userName: temp_users.get(0).get(1)
        })
    }
    let temp_list_struct = ShoppingListJson{
        listID: lid,
        name: temp_rows.get(0).get(0),
        shitToBuy: fooditem_res_vec,
        users: useritem_res_vec
    };*/