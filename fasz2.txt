  _________ ___ ___ .______________ ______________ ___    ________________  __      __________ __________ ____  __.  _________
 /   _____//   |   \|   \__    ___/ \__    ___/   |   \  /  _  \__    ___/ /  \    /  \_____  \\______   \    |/ _| /   _____/
 \_____  \/    ~    \   | |    |      |    | /    ~    \/  /_\  \|    |    \   \/\/   //   |   \|       _/      <   \_____  \
 /        \    Y    /   | |    |      |    | \    Y    /    |    \    |     \        //    |    \    |   \    |  \  /        \
/_________/\___|___/|___| |____|      |____|  \___|___/\____|__  /____|      \__/\__/ \_________/____|___/____|__ \/_________/
User mgmt ----------------

POST /register => Register user, also create a default fridge for it.
requestBody: {name:string; password:string}

POST /login => Log user in.
requestBody: {name: string; password: string}
responseBody: {token: string}

Fridge -------------------

GET /user/:userId/fridge => Get the fridge associated with the user, or a new one.
responseBody: { items: [...item] }

GET /user/:userId/fridge/connect/:id => Associate user with an existing fridge.

POST /user/:userId/fridge/add/:fridgeID => Add item to fridge
requestBody: { items: [...item] }

GET /user/:userid/fridge/clear/:fridgeID => kiüríti a hűtőt (igen, még a romlott kaját is kidobja)

GET /user/:userId/list/create/:listName  => Creates a new list with the specified name and adds it to the specified user

POST /user/:userId/list/update/:id => Update a list's content
requestBody: { items: [...item] }

DELETE /user/:userId/list/:id => Delete a list.

GET /user/:userId/list => Return all lists.
responseBody: { lists: [...list] }

GET /user/:userId/list/:id => Get a list with id.
responseBody: { items: [...item] }

List sharing -------------

POST /user/:userId/list/join/:listid => Add a user to a list.

GET /user/:uiserid/list/complete/:listid
requestbody {}
  _________ ___ ___ .______________    _____  .___  _________ _________.___ _______    ________
 /   _____//   |   \|   \__    ___/   /     \ |   |/   _____//   _____/|   |\      \  /  _____/
 \_____  \/    ~    \   | |    |     /  \ /  \|   |\_____  \ \_____  \ |   |/   |   \/   \  ___
 /        \    Y    /   | |    |    /    Y    \   |/        \/        \|   /    |    \    \_\  \
/________ /\___|_ _/|___| |____|    \____|__  /___/_________/_________/|___\____|____/\________/


ELFOGYTAK BAZMEG





